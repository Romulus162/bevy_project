use bevy::{ prelude::*, utils::HashMap };
// window::PrimaryWindow };
use bevy_rapier2d::prelude::{ Collider, RigidBody };
// use bevy_rapier2d::prelude::*;

use crate::animations::{ animate_sprites, Animations, AnimationTimer, AnimationIndices };

#[derive(Component, DerefMut, Deref)]
struct Velocity(Vec2);

#[derive(Component)]
pub struct Player;
#[derive(Component, Default, Debug, Clone, Copy)]
enum PlayerState {
    #[default]
    Idle,
    Running,
    Jumping,
    Falling,
}

#[derive(Component, Default, Debug, Clone, Copy)]
pub enum Facing {
    #[default]
    Right,
    Left,
}

#[derive(Component)]
struct PlayerDirection {
    direction: Vec2,
}

pub struct PlayerPlugin;

impl Plugin for PlayerPlugin {
    fn build(&self, app: &mut App) {
        app.add_startup_system(spawn_player)
            .add_system(change_player_animation.before(animate_sprites))
            .add_system(player_input)
            .add_system(player_movement);
    }
}

fn spawn_player(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    mut texture_atlases: ResMut<Assets<TextureAtlas>>
) {
    let animations = HashMap::from_iter(
        vec![
            (
                "idle".to_owned(),
                AnimationIndices {
                    handle: texture_atlases.add(
                        TextureAtlas::from_grid(
                            asset_server.load("Knight/Colour1/Outline/120x80_PNGSheets/_Idle.png"),
                            Vec2::new(120.0, 80.0),
                            10,
                            1,
                            None,
                            None
                        )
                    ),
                    last: 9,
                    first: 0,
                    fps: 12,
                },
            ),
            (
                "run".to_owned(),
                AnimationIndices {
                    handle: texture_atlases.add(
                        TextureAtlas::from_grid(
                            asset_server.load("Knight/Colour1/Outline/120x80_PNGSheets/_Run.png"),
                            Vec2::new(120.0, 80.0),
                            10,
                            1,
                            None,
                            None
                        )
                    ),
                    last: 9,
                    first: 0,
                    fps: 12,
                },
            ),
            (
                "jump".to_owned(),
                AnimationIndices {
                    handle: texture_atlases.add(
                        TextureAtlas::from_grid(
                            asset_server.load("Knight/Colour1/Outline/120x80_PNGSheets/_Jump.png"),
                            Vec2::new(120.0, 80.0),
                            3,
                            1,
                            None,
                            None
                        )
                    ),
                    last: 2,
                    first: 0,
                    fps: 12,
                },
            ),
            (
                "fall".to_owned(),
                AnimationIndices {
                    handle: texture_atlases.add(
                        TextureAtlas::from_grid(
                            asset_server.load("Knight/Colour1/Outline/120x80_PNGSheets/_Fall.png"),
                            Vec2::new(120.0, 80.0),
                            3,
                            1,
                            None,
                            None
                        )
                    ),
                    last: 2,
                    first: 0,
                    fps: 12,
                },
            )
        ]
    );

    let idle = animations["idle"].clone();
    let sprite_translation = Vec3::new(0.0, -230.0, 2.0);
    commands
        .spawn((Player, Collider::ball(15.0), RigidBody::Dynamic))
        .insert(Velocity(Vec2::new(0.0, 0.0)))
        .insert(PlayerState::default())
        .insert(PlayerDirection {
            direction: Vec2::new(0.0, 0.0),
        })
        .insert(Facing::default())
        .insert(SpriteSheetBundle {
            texture_atlas: idle.handle.clone(),
            sprite: TextureAtlasSprite::new(0),
            transform: Transform {
                translation: sprite_translation,
                scale: Vec3::splat(3.0),
                ..Default::default()
            },
            ..default()
        })
        .insert(AnimationTimer(Timer::from_seconds(1.0 / (idle.fps as f32), TimerMode::Repeating)));
    commands.insert_resource(Animations {
        map: animations,
        active: idle,
    });
}

fn player_movement(
    mut player_query: Query<(&Player, &mut Transform)>,
    keys: Res<Input<KeyCode>>
    // commands: Commands
) {
    let (_player, mut transform) = player_query.single_mut();

    if keys.pressed(KeyCode::W) {
        transform.translation.y += 5.0;
    }
    if keys.pressed(KeyCode::S) {
        transform.translation.y -= 0.1;
    }
    if keys.pressed(KeyCode::D) {
        transform.translation.x += 2.0;
    }
    if keys.pressed(KeyCode::A) {
        transform.translation.x -= 2.0;
    }
}

fn player_input(
    mut commands: Commands,
    keyboard_input: Res<Input<KeyCode>>,
    mut movement_query: Query<(Entity, &mut PlayerDirection, &PlayerState), With<Player>>,
    mut facing_query: Query<&mut Facing, With<Player>>
) {
    let Ok(mut facing) = facing_query.get_single_mut() else {
        return;
    };
    let mut player_movement = Vec2::new(0.0, 0.0);
    if keyboard_input.pressed(KeyCode::A) {
        player_movement.x -= 1.0;
        *facing = Facing::Left;
    }
    if keyboard_input.pressed(KeyCode::D) {
        player_movement.x += 1.0;
        *facing = Facing::Right;
    }
    if keyboard_input.pressed(KeyCode::W) {
        player_movement.y += 1.0;
    }
    if keyboard_input.pressed(KeyCode::S) {
        player_movement.y -= 10.0;
    }
    for (player, mut movement, state) in movement_query.iter_mut() {
        movement.direction = player_movement.normalize_or_zero();
        // if movement.direction.y > 0.0 {
        //     commands.entity(player).insert(PlayerState::Jumping);
        // } else if movement.direction.y < 0.0 {
        //     commands.entity(player).insert(PlayerState::Falling);
        if player_movement.y > 0.0 {
            if !matches!(state, PlayerState::Jumping) {
                commands.entity(player).insert(PlayerState::Jumping);
            }
        } else if movement.direction.y < 0.0 {
            if !matches!(state, PlayerState::Falling) {
                commands.entity(player).insert(PlayerState::Falling);
            }
        } else if player_movement.x != 0.0 {
            if !matches!(state, PlayerState::Running) {
                commands.entity(player).insert(PlayerState::Running);
            }
        } else if !matches!(state, PlayerState::Idle) {
            commands.entity(player).insert(PlayerState::Idle);
        }
    }
}

fn change_player_animation(
    mut commands: Commands,
    mut query: Query<(Entity, &PlayerState, Changed<PlayerState>), With<Player>>,
    mut animations: ResMut<Animations>
) {
    for (player, state, changed) in query.iter_mut() {
        if !changed {
            continue;
        }
        animations.active = (
            match state {
                PlayerState::Idle => &animations.map["idle"],
                PlayerState::Running => &animations.map["run"],
                PlayerState::Jumping => &animations.map["jump"],
                PlayerState::Falling => &animations.map["fall"],
            }
        ).clone();
        commands
            .entity(player)
            .insert((
                animations.active.handle.clone(),
                TextureAtlasSprite::new(animations.active.first),
                AnimationTimer(
                    Timer::from_seconds(1.0 / (animations.active.fps as f32), TimerMode::Repeating)
                ),
            ));
    }
}
