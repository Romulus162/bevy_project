use bevy::prelude::*;
use bevy_rapier2d::prelude::*;
// use bevy::{ prelude::*, window::PrimaryWindow };
mod environment;
mod movements;
mod enemy;
mod animations;
use bevy_rapier2d::prelude::{ RapierPhysicsPlugin, NoUserData };
use movements::PlayerPlugin;
// use enemy::EnemyPlugin;
use environment::{ floor, platform_one, spawn_background };
// use environment::MapPlugin;
use animations::AnimatorPlugin;
// use movements::direction_animations;
// use physics::{ setup_physics, print_ball_altitude };

// #[derive(Resource)]
// pub struct Health(pub usize);

// #[derive(States, Debug, Clone, Copy, Eq, PartialEq, Hash, Default)]
// pub enum AppState {
//     #[default]
//     MainMenu,
//     InGame,
//     YouDied,
// }

fn main() {
    App::new()
        .add_plugins(DefaultPlugins.set(ImagePlugin::default_nearest()))
        // .add_state::<AppState>()
        // .add_system(spawn_background)

        .add_startup_system(setup_camera)
        // .add_system(reset_health.in_schedule(OnEnter(AppState::InGame)))
        // .add_plugin(MapPlugin)

        //
        .add_startup_system(floor)

        .add_startup_system(platform_one)

        .add_plugin(AnimatorPlugin)
        .add_plugin(PlayerPlugin)
        // .add_plugin(EnemyPlugin)
        .add_plugin(RapierPhysicsPlugin::<NoUserData>::pixels_per_meter(50.0))

        // .add_system(direction_animations)

        .insert_resource(RapierConfiguration {
            gravity: Vec2::Y * -180.0,
            ..default()
        })
        .run();
}

fn setup_camera(mut commands: Commands) {
    commands.spawn(Camera2dBundle::default());
}

// fn reset_health(mut commands: Commands) {
//     commands.insert_resource(Health(100));
// }
