use bevy::{
    prelude::*,
    sprite::{ MaterialMesh2dBundle },
    window::PrimaryWindow,
    // render::view::RenderLayers,
};
use bevy_rapier2d::prelude::*;
// use crate::movements::Player;

// pub struct MapPlugin;

// impl Plugin for MapPlugin {
//     fn build(&self, app: &mut App) {
//         app.add_system(floor)
//             .add_system(ground_detection)
//             // .add_system(check_hit)
//             .add_system(spawn_map)
//             .add_system(ground_detection);
//     }
// }

pub fn floor(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<ColorMaterial>>
) {
    commands.spawn((
        RigidBody::Fixed,
        Collider::cuboid(1500.0, 85.0),
        MaterialMesh2dBundle {
            mesh: meshes.add(Mesh::from(shape::Quad::new(Vec2 { x: 1500.0, y: 85.0 }))).into(),
            material: materials.add(ColorMaterial::from(Color::MIDNIGHT_BLUE)),
            transform: Transform::from_translation(Vec3::new(0.0, -360.0, 2.0)),
            ..default()
        },
    ));
}

pub fn platform_one(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<ColorMaterial>>
) {
    commands.spawn((
        RigidBody::Fixed,
        Collider::cuboid(100.0, -40.0),
        MaterialMesh2dBundle {
            mesh: meshes.add(Mesh::from(shape::Quad::new(Vec2 { x: 200.0, y: -40.0 }))).into(),
            material: materials.add(ColorMaterial::from(Color::WHITE)),
            transform: Transform::from_translation(Vec3::new(500.0, -150.0, 2.0)),
            ..default()
        },
    ));
}

pub fn spawn_background(
    mut commands: Commands,
    // mut texture_atlases: ResMut<Assets<TextureAtlas>>,
    asset_server: Res<AssetServer>,
    window_query: Query<&Window, With<PrimaryWindow>>
) {
    let Ok(window) = window_query.get_single() else {
        return;
    };

    // Background
    commands.spawn(SpriteBundle {
        sprite: Sprite {
            custom_size: Some(Vec2::new(window.width(), window.height())),
            ..default()
        },
        texture: asset_server.load("Map/backgroundplatformer.png"),
        // transform: Transform::from_xyz(0.0, 0.0, 0.0),
        transform: Transform::from_xyz(0.0, 0.0, 1.0),

        ..default()
    });
}
