// use bevy::prelude::*;
// use crate::animations::{ AnimationIndices, AnimationTimer, animate_sprite };
// pub struct EnemyPlugin;
// impl Plugin for EnemyPlugin {
//     fn build(&self, app: &mut App) {
//         app.add_startup_system(player2).add_system(animate_sprite).add_system(player2_movement);
//     }
// }

// #[derive(Component)]
// pub struct Player2;

// fn player2_movement(
//     mut player_query: Query<(&Player2, &mut Transform)>,
//     keys: Res<Input<KeyCode>>
//     // transform: &mut Transform
// ) {
//     let (player, mut transform) = player_query.single_mut();

//     if keys.pressed(KeyCode::Up) {
//         transform.translation.y += 2.0;
//     }
//     if keys.pressed(KeyCode::Down) {
//         transform.translation.y -= 2.0;
//     }
//     if keys.pressed(KeyCode::Right) {
//         transform.translation.x += 2.0;
//     }
//     if keys.pressed(KeyCode::Left) {
//         transform.translation.x -= 2.0;
//     }
// }

// fn player2(
//     mut commands: Commands,
//     asset_server: Res<AssetServer>,
//     mut texture_atlases: ResMut<Assets<TextureAtlas>>
// ) {
//     let texture_handle = asset_server.load("Knight/Colour2/Outline/120x80_PNGSheets/_Idle.png");
//     let texture_atlas = TextureAtlas::from_grid(
//         texture_handle,
//         Vec2::new(120.0, 80.0),
//         10,
//         1,
//         None,
//         None
//     );
//     let texture_atlas_handle = texture_atlases.add(texture_atlas);
//     let animation_indices = AnimationIndices { first: 0, last: 9 };
//     let sprite_translation = Vec3::new(500.0, -200.0, 0.0);
//     commands.spawn((
//         Player2 {},
//         SpriteSheetBundle {
//             texture_atlas: texture_atlas_handle,
//             sprite: TextureAtlasSprite::new(animation_indices.first),
//             transform: Transform {
//                 translation: sprite_translation,
//                 scale: Vec3::splat(3.0),
//                 ..Default::default()
//             },
//             ..Default::default()
//         },
//         animation_indices,
//         AnimationTimer(Timer::from_seconds(0.1, TimerMode::Repeating)),
//     ));
// }
